import React, {useEffect} from 'react';
import {Route, Routes, Link, useParams, useNavigate, useLocation, Outlet} from "react-router-dom";
import './App.css'

const MainPage = () => {
    return (
        <div>
            <h1>This is MainPage</h1>
            <Link to='/page1'>go to Page 1</Link>
        </div>
    )
}
const Page1 = () => {
    return (
        <div>
            <h1>This is Page 1</h1>
            <div>
                <Link
                    onClick= {()=>console.log('test')}
                    className = 'link'
                    to='/'>
                    go to Main Page
                </Link>
            </div>

        </div>
    )
}
const ProductPage = () => {
    //this hook is to get the passed value
    let valuePassed = useParams();
    console.log('valuePassed', valuePassed);

    // useEffect(() => {
    //     fetch(url + valuePassed), []
    // })
    // fetch('/id= 1')
    const navigate = useNavigate();
    return (
        <div>
            <h1>This is Product Page, input id is {valuePassed.id}</h1>
            {/*useNav hook*/}
            <button onClick={() => navigate(-1)}>go back</button>
            <button onClick={() => navigate('/')}>Go to mainPage</button>
            {/*//windows methods---alternate method*/}
            <button onClick={() => window.history.go(-1)}>Window:Go -1</button>
            <button onClick={() => window.location.href = '/page2'}>Window:Go To Page2</button>
        </div>
    )
}
const NoMatch = () => {
    return (
        <div>
            <h2>Nothing to see here!</h2>
            <p><Link to='/'>Go to Page 1</Link></p>
        </div>
    )
}
const SharedLayout = () => {
    // useLocation can help you find the path
    const location = useLocation()
    console.log('location ==>', location)
    return (
        <>
            <nav style={
                {
                    backgroundColor: 'lightgreen',
                    display: 'flex',
                    justifyContent: 'space-around',
                    fontSize: '25px'
                }
            }>
                <Link to='/'>Main Page</Link>
                <Link to='/page1'>Page 1</Link>
                <Link to='/product-page/hello'>Product page</Link>
                {/*the function of Link is to help you jump to different urls*/}
                {/*<Link to={`/product-page/${clickedIndex}`}>Product page</Link>*/}
            </nav>
            <Outlet/>
            {/*this is the window where the component appears*/}
            <footer>
                <h2 style={{backgroundColor: 'lightgreen'}}>This is footer</h2>
            </footer>
        </>

    )
}
const App = () => {
    return (
        <Routes>
            <Route path="/" element={<SharedLayout/>}>
                <Route index element={<MainPage/>}/>
                <Route path='/page1' element={<Page1/>}/>
                {/*<Route path = '/page2' element = {<h2>This is page 2</h2>}/>*/}
                <Route path='/product-page/:musicId' element={<ProductPage/>}/>
                {/*define a page404*/}
                <Route path='*' element={<NoMatch/>}/>
            </Route>
        </Routes>
    );
}
//element renders in the comrresponding path url inside <Route/>
export default App;

//to pass a value to the destination page, 3 steps:
//1. in Routes, path = '/:key'
//2. in Link, pass /value
//3. In destination page, use hook (useParam) to obtain the value you passed

//to jump different pages through urls, 3 ways:
// 1. Link
// 2. useNavigate
// 3. window methods